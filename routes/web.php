<?php

use App\Http\Controllers\pageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route:: get('index',['as'=>'trangchu','uses'=>'pageController@getindex']);
Route::get('loaisanpham/{type?}',['as'=>'loaisanpham','uses'=>'pageController@getloaisp']);
Route::get('chitietsanpham',['as'=>'chitietsanpham','uses'=>'pageController@getchitiet']);
Route::get('lienhe',['as'=>'lienhe','uses'=>'pageController@getlienhe']);
Route::get('gioithieu',['as'=>'gioithieu','uses'=>'pageController@getgioithieu']);

