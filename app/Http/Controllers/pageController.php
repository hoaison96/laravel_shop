<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductType;
use App\Slide;
use Illuminate\Http\Request;

class pageController extends Controller
{
    public function getindex(){
        $slide= Slide::all();
        $newproduct=Product::where('new',1)->paginate(4);
        $khuyenmai=Product::where('promotion_price','<>',0)->paginate(4);

        return view('page.trangchu',compact('slide','newproduct','khuyenmai'));
    }
    public function getloaisp($type){
        $sp_theoloai=Product::where('id_type',$type)->get();
        $sp_khac=Product::where('id_type','<>',$type)->paginate(3);
        $loai=ProductType::all();
        $loaisp=ProductType::where('id',$type)->first();
        return view('page.loai_sanpham',compact('sp_theoloai','sp_khac','loai','loaisp'));
    }
    public function getchitiet(){
        return view('page.chitiet_sanpham');
    }
    public function getlienhe(){
        return view('page.contact');
    }
    public function getgioithieu(){
        return view('page.gioithieu');
    }
}
